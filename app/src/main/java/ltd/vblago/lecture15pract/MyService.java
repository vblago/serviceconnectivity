package ltd.vblago.lecture15pract;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import java.util.Random;

public class MyService extends Service {

    final Random random = new Random();
    MainActivity.ServiceConnectivity serviceConnectivity;

    MyBinder myBinder = new MyBinder();

    class MyBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }

    public void setServiceConnectivity(MainActivity.ServiceConnectivity serviceConnectivity) {
        this.serviceConnectivity = serviceConnectivity;
    }

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service start", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service stop", Toast.LENGTH_LONG).show();
    }

    public String getString() {
        return String.valueOf(random.nextInt());
    }

    public void onDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                serviceConnectivity.updateInfo(getString());
            }
        }, 5000);
    }
}
