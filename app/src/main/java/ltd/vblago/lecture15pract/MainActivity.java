package ltd.vblago.lecture15pract;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    MyService.MyBinder binder;
    MyService myService;
    boolean isBinded;

    @BindView(R.id.button)
    Button button;
    @BindView(R.id.button2)
    Button button2;


    public interface ServiceConnectivity {
        void updateInfo(String string);
    }

    ServiceConnectivity serviceConnectivity = new ServiceConnectivity() {
        @Override
        public void updateInfo(String string) {
            Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
        }
    };

    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Toast.makeText(getApplicationContext(), name.toString(), Toast.LENGTH_LONG).show();
            binder = (MyService.MyBinder) service;
            myService = binder.getService();
            myService.setServiceConnectivity(serviceConnectivity);
            button.setEnabled(true);
            button2.setEnabled(true);
            isBinded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBinded = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    public void mainToService() {
        Toast.makeText(this, myService.getString(), Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.button2)
    public void serviceToMain() {
        myService.onDelay();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MyService.class);
        bindService(intent, conn, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(conn);

    }

}
